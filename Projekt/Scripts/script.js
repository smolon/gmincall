﻿$(document).ready(function () {
    $(function () {
        function c() {
            p();
            var e = h();
            var r = 0;
            var u = false;
            l.empty();
            while (!u) {
                if (s[r] == e[0].weekday) {
                    u = true
                }
                else {
                    l.append('<div class="blank"></div>');
                    r++
                }
            }
            for (var c = 0; c < 42 - r; c++) {
                if (c >= e.length) {
                    l.append('<div class="blank"></div>')
                }
                else {
                    var v = e[c].day;
                    var m = g(new Date(t, n - 1, v)) ? '<div class="today">' : "<div>";
                    l.append(m + "" + v + "</div>")
                }
            }
            var y = o[n - 1];
            a.css("background-color", y).find("h1").text(i[n - 1] + " " + t);
            f.find("div").css("color", y);
            l.find(".today").css("background-color", y);
            d()
        }
        function h() {
            var e = [];
            for (var r = 1; r < v(t, n) + 1; r++) {
                e.push({ day: r, weekday: s[m(t, n, r)] })
            }
            return e
        }
        function p() {
            f.empty();
            for (var e = 0; e < 7; e++) {
                f.append("<div>" + s[e].substring(0, 3) + "</div>")
            }
        }
        function d() {
            var t;
            var n = $("#calendar").css("width", e + "px");
            n.find(t = "#calendar_weekdays, #calendar_content").css("width", e + "px").find("div").css({ width: Math.floor(e / 7) + "px", height: Math.floor(e / 7) + "px", "line-height": Math.floor(e / 7) + "px" });
            n.find("#calendar_header").css({ height: e * (1 / 7) + "px" }).find('i[class^="icon-chevron"]').css("line-height", e * (1 / 7) + "px")
        }
        function v(e, t) {
            return (new Date(e, t, 0)).getDate()
        }
        function m(e, t, n) {
            return (new Date(e, t - 1, n)).getDay()
        }
        function g(e) {
            return y(new Date) == y(e)
        }
        function y(e) {
            return e.getFullYear() + "/" + (e.getMonth() + 1) + "/" + e.getDate()
        }
        function b() {
            var e = new Date;
            t = e.getFullYear();
            n = e.getMonth() + 1
        }
        let element = document.querySelector('.container');
        let style = getComputedStyle(element);
        let docwidth = parseInt(style.width);
        let qq = parseInt(docwidth * (1 / 4) * 0.89 / 7);
        var e = qq * 7;
        var t = 2013;
        var n = 9;
        var r = [];
        var i = ["JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER"];
        var s = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var o = ["#16a085", "#1abc9c", "#c0392b", "#27ae60", "#FF6860", "#f39c12", "#f1c40f", "#e67e22", "#2ecc71", "#e74c3c", "#d35400", "#2c3e50"];
        var u = $("#calendar"); var a = u.find("#calendar_header");
        var f = u.find("#calendar_weekdays");
        var l = u.find("#calendar_content");
        b();
        c();
        a.find('i[class^="icon-chevron"]').on("click", function () {
            var e = $(this);
            var r = function (e) {
                n = e == "next" ? n + 1 : n - 1;
                if (n < 1) { n = 12; t-- }
                else if (n > 12) {
                    n = 1;
                    t++
                }
                c()
            };
            if (e.attr("class").indexOf("left") != -1) {
                r("previous")
            }
            else {
                r("next")
            }
        })
    })

    $(document).ready(function () {
        let latszud = "53.2987222";
        let longszud = "23.6558889";
        //https://openweathermap.org/weather-conditions  link do wszyskich mozliwych opisow pogody
        var api = 'https://fcc-weather-api.glitch.me/api/current?lat=' + latszud + '&lon=' + longszud + '';

        $.getJSON(api, function (res) {

            var celsius = res.main.temp;
            var farenheit = (celsius * 1.8) + 32;
            var location = "Szudziałowo";



            $('.weather-location').html(location);
            $('.temp').html(Math.floor(celsius));
            $('.weather-description').html(res.weather[0].description);
            $('.weatherType').attr('id', res.weather[0].main);
            $('.temp, .temp-type').on('click', function () {
                if ($('.temp').html() == (Math.floor(celsius))) {
                    $('.temp').html(Math.floor(farenheit));
                    $('.temp-type').html('°F');

                } else {
                    $('.temp').html(Math.floor(celsius));
                    $('.temp-type').html('°C');
                }
            });


            //SETTING UP THE ICON 
            var icons = new Skycons({
                "color": "#333"
            });

            icons.set("Clear", Skycons.CLEAR_DAY);
            icons.set("Clear-night", Skycons.CLEAR_NIGHT);
            icons.set("Partly-cloudy-day", Skycons.PARTLY_CLOUDY_DAY);
            icons.set("Partly-cloudy-night", Skycons.PARTLY_CLOUDY_NIGHT);
            icons.set("Clouds", Skycons.CLOUDY);
            icons.set("Rain", Skycons.RAIN);
            icons.set("Sleet", Skycons.SLEET);
            icons.set("Snow", Skycons.SNOW);
            icons.set("Wind", Skycons.WIND);
            icons.set("Fog", Skycons.FOG);
            icons.play();

        });
    });
});