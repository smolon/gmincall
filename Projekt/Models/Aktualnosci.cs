﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Projekt.Models
{
    public class Aktualnosci
    {
        [ScaffoldColumn(false)]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Tytuł")]
        [Required(ErrorMessage = "Musisz wprowadzić nazwę")]
        [StringLength(30)]
        public string Title { get; set; }

        [Display(Name = "Prześlij plik")]
        public string ImagePath { get; set; }
        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "Opis")]
        // [Required(ErrorMessage = "Musisz podać opis")]
        public string Description { get; set; }

        [Display(Name = "Data wydarzenia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? Date { get; set; }
    }
}