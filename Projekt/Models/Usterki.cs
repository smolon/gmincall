﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Projekt.Models
{
    public class Usterki
    {
        [ScaffoldColumn(false)]
        [Key]
        public int idUsterki { get; set; }

        [Display(Name = "Rodzaj/typ usterki")]
        public Typ typUsterki { get; set; }

        [Display(Name = "Opis")]
        public string opis { get; set; }

        [Display(Name = "Data zgłoszenia")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? dataZgloszenia { get; set; }
        
        [Display(Name = "Prześlij plik")]
        public string ImagePath { get; set; }
        [NotMapped]
        public HttpPostedFileBase ImageFile { get; set; }

        [Display(Name = "Miejsce")]
         public string wspolrzedne { get; set; }
        [ScaffoldColumn(false)]
        public int? status { get; set; }
    }
    public enum Typ
    {
        hydrauliczne,
        drogowe,
        elektryczne,
        inne
    }
}