﻿using Projekt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Projekt.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View(db.aktualnosci.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize]
        public ActionResult DodajPost()
        {
            Aktualnosci post = new Aktualnosci();
            return View(post);
        }
        [Authorize]
        [HttpPost]
        public ActionResult DodajPost(Aktualnosci post)
        {

            if (!ModelState.IsValid)
            {
                return View(post);
            }
            else
            {
                string fileName = Path.GetFileNameWithoutExtension(post.ImageFile.FileName);
                string extension = Path.GetExtension(post.ImageFile.FileName);
                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                post.ImagePath = "~/Image/" + fileName;
                fileName = Path.Combine(Server.MapPath("~/Image/"), fileName);
                post.ImageFile.SaveAs(fileName);
                //foreach (var item in db.News)
                //{
                //    if (post.Equals(item))
                //    {
                //        ViewBag.Message = "Istnieje taki post!";
                //        return View();
                //    }
                //}
                ViewBag.Message = "Dodano post " + post.Title;
                db.aktualnosci.Add(post);
                db.SaveChanges();
                //ModelState.Clear();
                return View();
            }
        }
    }
}